const thrift = require('thrift');

const localServer = {host: 'localhost', port: 9090};
const remoteServer = {/* production configuration */};
const connection = {transport: thrift.TBufferedTransport, protocol: thrift.TBinaryProtocol};
const apiKey = '97423438d432495db1aa789b8f180000';

module.exports = {
  environment: 'development',
  development: {
    client: {...localServer, ...connection},
    server: localServer,
    apiKey,
  },
  production: {
    client: {...remoteServer, ...connection},
    server: remoteServer,
    apiKey,
  },
};
