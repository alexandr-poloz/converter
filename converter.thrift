struct Work {
  1: double value,
  2: string input,
  3: string output
}

exception InvalidOperation {
  1: string message
}

service Converter {

   void ping(),

   double run(1:Work work) throws (1:InvalidOperation ouch)

}
