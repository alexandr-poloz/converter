module.exports = {
  "extends": "google",
  "parserOptions": {
    "ecmaVersion": 7,
  },
  "rules": {
    "max-len": ["error", {
      "code": 120,
      "tabWidth": 2,
      "ignoreUrls": true,
    }],
  },
  "parser": "babel-eslint"
};