const ERROR_CONFIGURATION_FILE = 'Could not open the configuration file';
const ERROR_ENVIRONMENT_IS_EMPTY = 'Environment not defined';
const ERROR_SERVER_CONNECTION = 'Error connecting to server';

module.exports = {
  ERROR_CONFIGURATION_FILE,
  ERROR_ENVIRONMENT_IS_EMPTY,
  ERROR_SERVER_CONNECTION,
};
