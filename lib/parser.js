const template = /^([0-9.]+)([A-z]{3})to([A-z]{3})$/;

/**
 * Get request string from ARGV
 * @return {string}
 */
function getRequestString() {
  const {argv=[]} = process;
  return argv.slice(2).join('');
}

/**
 * @param {String | Number} value
 * @return {Number}
 */
function toFixed(value) {
  const fixed = parseFloat(value).toFixed(2);
  return parseFloat(fixed);
}

/**
 * Parse request string from ARGV
 * @return {{value: Number, input: String, output: String}}
 */
function parseARGV() {
   const request = getRequestString();
   const result = template.exec(request);
   if (result instanceof Array) {
     let [, value, input, output] = result;
     return {value: toFixed(value), input, output};
   }
   throw new Error("Invalid");
}

module.exports = parseARGV;
