const parseARGV = require('./parser');
const {ping, work, close} = require('./client');

/**
 * Run conversion
 * @return {Promise}
 */
async function converter() {
  const request = parseARGV();
  const {value, input, output} = request;
  logRequest(request);
  await ping();
  const response = await work(value, input, output);
  return {value, input, output, response};
}

/**
 * Write request to log
 * @param {*} data
 */
function logRequest(data) {
  const {value, input, output} = data;
  console.log(`Conversion ${value} ${input} to ${output}`);
  console.log(`Please, wait.....`);
}

/**
 * Write response to log
 * @param {*} data
 */
function logResponse(data) {
  const {value, input, output, response} = data;
  console.log(`Response: ${value} ${input} = ${response} ${output}`);
}

converter().then((data) => {
  logResponse(data);
  close();
}).catch((err) => {
  console.log(err.message);
  close();
});
