const thrift = require('thrift');
const Converter = require('../gen-nodejs/Converter');
const exchange = require('./exchange');
const {server: {port, host}} = require('./config');

const server = thrift.createServer(Converter, {
  /**
   * @param {Function} cb
   */
  ping(cb) {
    cb(null);
  },

  /**
   * @param {Work} work
   * @param {Function} cb
   */
  run(work, cb) {
    exchange(work).then((rate) => {
      cb(null, rate.toFixed(2));
    }).catch(() => {
      cb({message: 'Unexpected server error!'});
    });
  },
});

server.listen(port, host);
