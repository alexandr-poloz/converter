const thrift = require('thrift');
const Converter = require('../gen-nodejs/Converter');
const types = require('../gen-nodejs/converter_types');
const {ERROR_SERVER_CONNECTION} = require('./errors');

/** Create connection */
const {server: {port, host, transport, protocol}} = require('./config');
const connection = thrift.createConnection(host, port, {transport, protocol});
connection.on('error', () => {
  close();
  throw new Error(ERROR_SERVER_CONNECTION);
});

/** Create client */
const client = thrift.createClient(Converter, connection);

/**
 * Check connection
 * @return {Promise}
 */
function ping() {
  return new Promise((resolve, reject) => {
    client.ping((err) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
}

/**
 * Run remote exchange calculation
 * @param {Number} value
 * @param {String} input
 * @param {String} output
 * @return {Promise.<Number>}
 */
function work(value, input, output) {
  return new Promise((resolve, reject) => {
    const work = new types.Work({value, input, output});
    client.run(work, (err, response) => {
      if (err) {
        reject(err);
      } else {
        resolve(response);
      }
    });
  });
}

/**
 * Close connection
 */
function close() {
  connection.end();
}

module.exports = {ping, work, close};
