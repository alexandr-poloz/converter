const exchangeRates = require('exchange-rates');
const money = require('money');
const {apiKey} = require('./config');

/**
 * Configuration for a open exchange service
 * @type {string}
 */
exchangeRates.url = `https://openexchangerates.org/api/latest.json?app_id=${apiKey}`;

/**
 * Load the API data from https://openexchangerates.org
 * @return {Promise}
 */
function loadRates() {
  return new Promise((resolve, reject) => {
    try {
      exchangeRates.load((err) => {
        console.log(exchangeRates);
        money.rates = exchangeRates.rates;
        money.base = exchangeRates.base;
        resolve();
      });
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * Exchange method
 * @param {Work} work
 * @return {Promise.<Number>}
 */
function exchange(work) {
  return loadRates().then(() => {
    const {value, input, output} = work;
    return money(value).from(input).to(output);
  });
}

module.exports = exchange;
