const fs = require('fs');
const path = require('path');

const {ERROR_CONFIGURATION_FILE, ERROR_ENVIRONMENT_IS_EMPTY} = require('./errors');

/**
 * Check if exist the configuration file
 * And read configuration data
 * @return {*}
 */
function readConfigFile() {
  const configurationPath = path.join(__dirname, '../env.js');
  const isExistsConfigFile = fs.existsSync(configurationPath);

  if (!isExistsConfigFile) {
    throw new Error(ERROR_CONFIGURATION_FILE);
  } else {
    return require(configurationPath);
  }
}

/**
 * Get configuration data for active environment
 * @param {{}} configuration
 * @return {{client: {}, server: {}}}
 */
function getEnvironment(configuration) {
  const {environment} = configuration;
  if (!environment || configuration[environment] === undefined) {
    throw new Error(ERROR_ENVIRONMENT_IS_EMPTY);
  } else {
    return configuration[environment];
  }
}

const configuration = readConfigFile();
const environment = getEnvironment(configuration);

module.exports = environment;
